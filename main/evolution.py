import ast
import deap as ea
import logging
import json
import numpy as np
import os
import pickle
import random
import ray
import string

from deap import creator, base, tools, algorithms
from functools import partial
from time import sleep, time
from random import sample

from main.fitness_functions import eval_dsm_min_coordination

ALPHA_MATRIX = [0.2, 0.4, 0.6]
BETA_MATRIX = [0.6, 0.3, 0.2]
OPTIMIZE_TYPES = ["Lean", "Balanced", "Independent"]
logging.basicConfig(
    filemode="a",
    format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
    datefmt="%m-%dT%H:%M:%S",
    level=logging.DEBUG,
)

logging.info("Running Ray cluster")
ray_logger = logging.getLogger("ray")


class Evolution:
    def __init__(
        self,
        analysis_params,
        dsm=None,
        filter_role=None,
        filter_weights=0,
        survey_type=None,
        survey_params=None,
        earun=True,
        ongoing=True,
    ):
        """

        :param dsm: pandas dataframe
        :param earun: flag to run evolution algorithm
        :param filter_role: Set any text value here matching an activity to only keep those activities
         ex:    ['Planlegging eller gjennomføring av undervisningstiltak']
                ['Planlegging eller gjennomføring av forskningsprosjekter']
                ['Administrasjon og drift', 'Planlegging eller gjennomføring av undervisningstiltak']
        :param filter_weights: Any weights below or equal to this value will be set to 0 in the DSM matrix
        """
        self.filter_role = filter_role or []
        self.filter_weights = filter_weights
        self.survey_type = survey_type
        self.analysis_id = analysis_params["id"]
        self.ongoing = ongoing

        if dsm is None:
            raise ValueError("DSM dataframe is required!")
        else:
            pickle_file = f"direct_input_{self.analysis_id}"

        # Parameters for the evolutionary algorithm ###

        # self.dsm_to_eval = self._convert_dsm(dsm.copy())

        # dsm_eval_list to make it independent of function
        # which is looped gen x population size number of times
        self.dsm_eval_list = dsm.to_numpy()

        #self.dsm_head = self.dsm_to_eval.columns
        #if self.dsm_head[0] == "," or self.dsm_head[0] == "":
        #    self.dsm_head = self.dsm_head[1:]

        self.dsm_elem_count = len(self.dsm_eval_list)  # NUMBER_OF_DSM_ELEMENTS

        self.alpha = ALPHA_MATRIX[analysis_params["optimize_type"] - 1]
        self.beta = BETA_MATRIX[analysis_params["optimize_type"] - 1]

        if earun:
            self.MAX_NUMBER_OF_CLUSTERS = analysis_params["max_clusters"]
            self.checkpoint_file = pickle_file + "_checkpoint.pkl"

            # params for GA
            self.genome_len = self.dsm_elem_count
            self.POPULATION_SIZE = survey_params["param_population"]
            self.NUMBER_OF_GENERATIONS = analysis_params["number_of_generations"]

            print("number_of_generations:", self.NUMBER_OF_GENERATIONS)
            self.cxpb = survey_params["param_cxpb"]  # Crossover probability
            self.mutpb = survey_params["param_mubp"]  # Mutation probability
            self.progress_per_evolution = 0

            # Moving evolution fitness creation into function so we can run setup on ray actors
            def creator_setup():
                ea.creator.create("FitnessMin", ea.base.Fitness, weights=(-1.0,))
                ea.creator.create("Individual", list, fitness=ea.creator.FitnessMin)

            creator_setup()

            self.toolbox = ea.base.Toolbox()

            # Setting resources(memory, object_store_memory) helps
            # to ensure consistent results.
            # View the Ray dashboard at localhost:8265 for more info
            # if unsure(start with plain ray.init()))
            if not ray.is_initialized():
                ray.init(
                    address="auto",
                    _redis_password="5241590000000000",
                    log_to_driver=False,
                )

            self.toolbox.register("map", self.ray_deap_map, creator_setup=creator_setup)

            # An individual is defined as a list of cluster assignments.
            # Each index matches the index of the DSM elements.
            # If the individual has index 2 set to 3 that means that element at
            # index 2 in the DSM is assigned to cluster 3.
            self.toolbox.register(
                "attribute", random.randint, 0, self.MAX_NUMBER_OF_CLUSTERS - 1
            )
            # pool = Pool(2)
            # self.toolbox.register("map", pool.map)
            self.toolbox.register(
                "individual",
                tools.initRepeat,
                creator.Individual,
                self.toolbox.attribute,
                n=self.genome_len,
            )
            self.toolbox.register(
                "population", tools.initRepeat, list, self.toolbox.individual
            )

            self.toolbox.register("evaluate", eval_dsm_min_coordination)
            self.toolbox.register("mate", tools.cxTwoPoint)
            self.toolbox.register(
                "mutate",
                tools.mutUniformInt,
                indpb=survey_params["param_indpb"],
                low=0,
                up=self.MAX_NUMBER_OF_CLUSTERS - 1,
            )
            # Use elitism + normal ranking
            self.toolbox.register(
                "select",
                tools.selTournament,
                tournsize=survey_params["param_tournsize"],
            )
            self.stats = tools.Statistics(lambda ind: ind.fitness.values)
            self.stats.register("avg", np.mean)
            self.stats.register("min", np.min)
            self.stats.register("max", np.max)

    def run_evolution(self):
        evo_start = time()
        hall_of_fame = tools.HallOfFame(2)
        population = self.toolbox.population(n=self.POPULATION_SIZE)
        tools.initIterate(
            list,
            partial(
                sample, range(self.MAX_NUMBER_OF_CLUSTERS), self.MAX_NUMBER_OF_CLUSTERS,
            ),
        )

        properties = {
            "dsm": self.dsm_eval_list,
            "max_clusters": self.MAX_NUMBER_OF_CLUSTERS,
            "alpha": self.alpha,
            "beta": self.beta,
        }

        # Setting all values in matrix to 1
        # Simplifying the problem, but also ignoring the details of the relationships
        properties["dsm"][properties["dsm"] > 1] = 1

        # Put DSM matrix and attached properties in Ray shared object storage
        # Pass the ID for the stored object to each worker so they know where to find it
        properties_id = ray.put(properties)

        # If this analysis has a cehckpoint we start from there
        if not self.ongoing:
            print("Starting from beginning")
            gen = 0
        elif os.path.isfile(self.checkpoint_file):
            checkpoint = pickle.load(open(self.checkpoint_file, "rb"))
            population = checkpoint["population"]
            gen = (
                checkpoint["generation"] + 1
            )  # +1 to avoid repeating the generation that was stored
            random.setstate(checkpoint["rndstate"])
            print("Checkpoint found, starting from generation ", gen)
        else:
            print("No checkpoint found, starting from scratch")
            gen = 0

        while gen < self.NUMBER_OF_GENERATIONS:
            gen_start = time()

            # Update population
            population = self.toolbox.select(population, k=len(population))
            population = [self.toolbox.clone(ind) for ind in population]
            population = ea.algorithms.varAnd(
                population, self.toolbox, cxpb=self.cxpb, mutpb=self.mutpb
            )

            offspring = [
                individual for individual in population if not individual.fitness.valid
            ]

            fits = self.toolbox.map(self.toolbox.evaluate, offspring, properties_id)
            for fit, ind in zip(fits, offspring):
                ind.fitness.values = fit

            hall_of_fame.update(offspring)
            if gen % 10 == 0 and gen > 0:
                print(f"Generation {gen}: complete in: {time() - gen_start}s")

            # Checkpoint the evolution
            if gen % 100 == 0 and gen > 0:
                checkpoint = dict(
                    population=population, generation=gen, rndstate=random.getstate(),
                )
                pickle.dump(checkpoint, open(self.checkpoint_file, "wb"), 4)
                # pickle.dump(logbook, open(checkpoint_logbook_file, "wb"), 4)
                print(f"Checkpoint: {self.checkpoint_file} at generation {gen}")

            gen += 1

        print("Evolution completed in:", time() - evo_start, "seconds")
        return hall_of_fame[0].copy()

    # This is what we register as map in deap toolbox.
    # For GA no need to provide pset_creator. Both needed for GP
    def ray_deap_map(
        self, func, pop, properties_id, creator_setup=None, pset_creator=None
    ):
        # Manager will determine if batching is needed and crate remote actors
        # to do work
        map_ray_manager = Ray_Deap_Map_Manager(creator_setup, pset_creator)
        results = map_ray_manager.map(func, pop, properties_id)

        return results


@ray.remote(num_cpus=1)
class Ray_Deap_Map:
    def __init__(self, creator_setup=None, pset_creator=None):
        # issue 946? Ensure non trivial startup to prevent bad load
        # balance across a cluster
        sleep(0.01)

        # recreate scope from global
        # For GA no need to provide pset_creator. Both needed for GP
        self.creator_setup = creator_setup
        if creator_setup is not None:
            self.creator_setup()

        self.pset_creator = pset_creator
        if pset_creator is not None:
            self.pset_creator()

    def ray_remote_eval_batch(self, f, zipped_input):
        iterable, id_, properties_id = zipped_input
        # attach id so we can reorder the batches
        obj_start = time()
        # Retrieve properties object from Ray Shared Object Storage
        properties = ray.get(properties_id)
        ray_logger.info(f"reading obj storage took: {time() - obj_start}s")

        return [(f(i, properties), id_) for i in iterable]


class Ray_Deap_Map_Manager:
    def __init__(self, creator_setup=None, pset_creator=None):
        # Can adjust the number of processes in ray.init or when launching cluster
        self.n_workers = int(ray.available_resources().get("CPU") or 1)
        if self.n_workers > 4:
            self.n_workers = self.n_workers - 2
        ray_logger.info(f"n_workers: {self.n_workers}")

        # recreate scope from global (for ex need toolbox in gp too)
        self.creator_setup = creator_setup
        self.pset_creator = pset_creator

    def map(self, func, iterable, properties_id):
        min_batch_size = 50
        map_start = time()

        # many workers, lets use ActorPool
        if len(iterable) / self.n_workers < min_batch_size:
            n_workers = int(len(iterable) / min_batch_size) or 1
        else:
            n_workers = self.n_workers

        ray_logger.info(f"n_workers: {n_workers}")
        ray_logger.info(f"len(iterable): {len(iterable)}")
        n_per_batch = int(len(iterable) / n_workers) + 1

        ray_logger.info(f"n_per_batch: {n_per_batch}")
        batches = [
            iterable[i : i + n_per_batch]
            for i in range(0, len(iterable), n_per_batch)
        ]
        id_for_reorder = range(len(batches))

        fitness_eval_start = tmp = time()
        ray_logger.info(f"Batches {len(batches)}")

        actor_pool = [
            Ray_Deap_Map.remote(self.creator_setup, self.pset_creator)
            for _ in range(n_workers)
        ]
        ray_logger.info(f"Actors took: {time() - tmp}")
        tmp = time()
        value_list = list(
            zip(batches, id_for_reorder, [properties_id for _ in batches])
        )

        fitness_futures = []
        for actor, inputs in zip(actor_pool, value_list):
            fitness_futures.append(actor.ray_remote_eval_batch.remote(func, inputs))

        unordered_fitness_list = ray.get(
            fitness_futures
        )
        ray_logger.info(f"Got fitness: {time() - tmp}")
        tmp = time()

        ray_logger.info(f"Fitness evaluation took: {time() - fitness_eval_start}s")
        # ensure order of batches
        ordered_fitness_results = [
            batch
            for batch_id in id_for_reorder
            for batch in unordered_fitness_list
            if batch_id == batch[0][1]
        ]

        # flatten batches to list of fitness
        results = [
            item[0] for sublist in ordered_fitness_results for item in sublist
        ]
        ray_logger.info(f"Got result: {time() - tmp}")
        tmp = time()
        for actor_handle in actor_pool:
            ray.kill(actor_handle)
        ray_logger.info(f"Killed actors: {time() - tmp}")
        print("Map executed in:", time() - map_start, "seconds")
        return results
