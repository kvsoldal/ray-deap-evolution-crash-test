import math
import itertools
import numpy as np
from collections import Counter


def eval_dsm_min_coordination(individual, properties):
    count_per_cluster = Counter(individual)
    nc = len(count_per_cluster)
    nn = len(properties["dsm"])
    department_mask = np.reshape(np.ma.make_mask([r[0] == r[1] for r in itertools.product(individual, individual)]),
                                 (len(individual), len(individual)))

    s1 = np.sum(np.ma.masked_array(properties["dsm"], department_mask))
    s2 = np.count_nonzero(np.ma.masked_array(properties["dsm"], np.logical_not(department_mask)) == 0) - nn

    mdl_weight = 1 - properties["alpha"] - properties["beta"]
    mdl = nc * math.log(nn, 2) + (math.log(nn, 2) * sum(count_per_cluster.values()))
    type_error_score = properties["alpha"] * (s1 * (2 * math.log(nn + 1, 2))) + \
                       properties["beta"] * (s2 * (2 * math.log(nn + 1, 2)))

    fitness = mdl_weight * mdl + type_error_score
    print("fitness", fitness)
    return [fitness]