from main.dsm_eval import *
from main.evolution import Evolution

dsm = convert_dsm(json_to_unordered_df(dsm_eval()))
dsm[dsm > 1] = 1

analysis_params = {
    "id": 0,
    "max_clusters": 53,
    "optimize_type": 2,
    "number_of_generations": 100000,
}

survey_params = {
    "param_population": 1000,
    "param_cxpb": 0.5,
    "param_mubp": 0.1,
    "param_indpb": 0.05,
    "param_tournsize": 10,
}
evolution = Evolution(analysis_params, dsm=dsm, survey_params=survey_params)
print(evolution.run_evolution())


